# Secure files

Just various pieces of code that I've written for file and string encryption.

I will re-use it at some point, so leaving here for future-me.

## Author

Ryan Weber, One Smart Space Ltd. © 2023 All rights reserved.
