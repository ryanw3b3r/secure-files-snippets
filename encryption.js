const encryption = {
    name: 'AES-CBC'
}

export function generateKey() {
    return crypto.subtle.generateKey({ name: encryption.name, length: 256 }, true, ['encrypt', 'decrypt'])
        .then(generatedKey => generatedKey)
        .catch(console.error)
}

export function encrypt(key, data) { /* Returns ArrayBuffer */
    const iv = crypto.getRandomValues(new Uint8Array(16))
    return crypto.subtle.encrypt({ name: encryption.name, iv }, key, data)
        .then(file => ({ file, iv }))
        .catch(console.error)
}

export function getKeyAsJson(key) {
    return crypto.subtle.exportKey('jwk', key)
        .then(keyAsJson => keyAsJson.k)
            /* {
                    "alg":"A256CBC",
                    "ext":true,
                    "k":"WJxq-RnGU6MCMIX0wXXxXi2ZPR1XRG0h7HzcjpZP9Gw",
                    "key_ops":["encrypt","decrypt"],
                    "kty":"oct"
                }
            */
}
