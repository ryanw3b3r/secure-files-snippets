import Vue from 'vue'
import Axios from 'axios'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
    load: {
        key: document.querySelector('meta[name="google-maps-key"]')?.getAttribute('content'),
        region: 'GB',
        language: 'en',
    },
    installComponents: true
})

Vue.component('google-map', VueGoogleMaps.Map)
Vue.component('google-marker', VueGoogleMaps.Marker)
Vue.component('secure-text-form', require('./components/SecureTextForm').default)
Vue.component('secure-file-form', require('./components/SecureFileForm').default)
Vue.component('contact-us-form', require('./components/ContactUsForm').default)
Vue.prototype.$http = Axios

const vue = new Vue({
    el: '#app',
    data: {
        topMenuMobileOn: false,
        topMenuSecurityFlyOutOn: false,
        windowWidth: 0,
    },
    computed: {
        google: VueGoogleMaps.gmapApi
    },
    mounted() {
        this.$nextTick(function() {
            window.addEventListener('resize', this.getWindowWidth);
            this.getWindowWidth()
        })
    },
    methods: {
        getWindowWidth(event) {
            this.windowWidth = document.documentElement.clientWidth;
        },
    },
    beforeDestroy() {
        window.removeEventListener('resize', this.getWindowWidth);
    }
})

vue.$http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

export default vue
