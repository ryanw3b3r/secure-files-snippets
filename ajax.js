import vue from './Vue'
import Payload from './classes/Payload'

export function sendFile(encrypted, expiryTime) {
    const payload = new Payload

    payload.addEncryptedFile('hash', encrypted).add('expires', expiryTime)

    console.log(payload, payload.data)

    vue.$http.post('/save', payload.data, { headers: { 'Content-Type': 'multipart/form-data' } })
        .then(
            response => {
                console.log(response)
                this.response = `Your link is: ${response.data.link}`
            },
            badResponse => console.log(badResponse)
        )
}
