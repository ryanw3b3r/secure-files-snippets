export default class Payload {
    constructor() {
        this._data = new FormData
    }

    get data() {
        return this._data
    }

    addEncryptedFile(key, encrypted) {
        const base64version = btoa(
            Array.from(new Uint8Array(encrypted.file))
                .map(byte => String.fromCharCode(byte))
                .join('')
        )

        const ivHex = Array.from(encrypted.iv)
            .map(b => ('00' + b.toString(16)).slice(-2))
            .join('')

        this._data.append(key, ivHex + base64version)
        return this
    }

    add(key, value) {
        console.log('Add method', key, value, 'End of Add method')
        this._data.append(key, value)
        return this
    }
}
